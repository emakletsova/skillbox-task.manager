<?php
include($_SERVER['DOCUMENT_ROOT'] . "/include/init.php");

include($_SERVER['DOCUMENT_ROOT'] . "/include/auth.php");
ob_start();

$title = "Возможности проекта —";
$title_text = "Вести свои личные списки, например покупки в магазине, цели, задачи и многое другое. Делится списками с друзьями и просматривать списки друзей.";
include($_SERVER['DOCUMENT_ROOT'] . "/include/menu.php");
include($_SERVER['DOCUMENT_ROOT'] . "/template/header.php");
include($_SERVER['DOCUMENT_ROOT'] . "/template/footer.php");
ob_end_flush();
