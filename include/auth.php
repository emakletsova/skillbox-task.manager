<?php

include($_SERVER['DOCUMENT_ROOT'] . "/include/db_user.php");

if (isset($_POST["unauth"]))
{
   logout();
}

function logout(){
    session_unset();
    session_destroy();
    if (isset($_COOKIE['login'])) {
        unset($_COOKIE['login']);
        setcookie('login', '', 1, '/'); // empty value and old timestamp
    }
}
function isUserAuth(){
    return $_SESSION["auth"]??false;
}
function printLogoutForm(){
    if(isUserAuth()) {
        include($_SERVER['DOCUMENT_ROOT'] . "/template/logout.php");
        setcookie("login", $_COOKIE['login'], time() + 30 * 24 * 60 * 60,  "/");
    }
}
function printAuthForm()
{
    printLogoutForm();
    if (checkIsCurrentPath("/") && !($_SESSION["auth"] ?? false)) {
        $checkIsGood = null;
        if (isset($_POST["auth"])) {
            $checkIsGood = checkUser($_POST["login"], $_POST["password"]);
        }
        if (isset($_GET['login']) && $_GET['login'] == 'yes') {
            if ($checkIsGood) {
                include($_SERVER['DOCUMENT_ROOT'] . "/template/success.html");
                $_SESSION["auth"] = true;
                $_SESSION["userId"] = getUserByLogin($_POST["login"])['id'];
                setcookie("login", $_POST["login"], time() + 30 * 24 * 60 * 60,  "/");
            } else {
                if ($checkIsGood === false)
                    include($_SERVER['DOCUMENT_ROOT'] . "/template/error.html");
                if (isset($_COOKIE['login']))
                    include($_SERVER['DOCUMENT_ROOT'] . "/template/auth_short.php");
                else
                    include($_SERVER['DOCUMENT_ROOT'] . "/template/auth.php");
            }
        }
    }
}
