<?php
    static $arrMenu = [
        [
            'title' => 'Главная',
            'path' => '/',
            'sort' => 1,
        ],
        [
            'title' => 'О нас',
            'path' => '/route/about/',
            'sort' => 2,
        ],
        [
            'title' => 'Контакты',
            'path' => '/route/contacts/',
            'sort' => 3,
        ],
        [
            'title' => 'Почему мы',
            'path' => '/route/whyus/',
            'sort' => 4,
        ],
        [
            'title' => 'Наша команда',
            'path' => '/route/ourteam/',
            'sort' => 6,
        ],
        [
            'title' => 'Наши классные проекты',
            'path' => '/route/ourprojects/',
            'sort' => 5,
        ],
        [
            'title' => 'Сообщения',
            'path' => '/posts/',
            'sort' => 7,
        ],
    ];
