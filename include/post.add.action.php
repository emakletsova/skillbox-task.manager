<?php
ini_set('session.gc_maxlifetime', 20 * 60);
session_set_cookie_params(20 * 60);
ini_set('session.cookie_lifetime', 20 * 60);
session_name("session_id");
session_start();
if (empty($_SESSION["auth"]))
    header("Location: /?login=yes");
include($_SERVER['DOCUMENT_ROOT'] . "/include/auth.php");

$user = getUserById($_SESSION["userId"]);
include($_SERVER['DOCUMENT_ROOT'] . "/include/db_posts.php");
$header = $_POST["header"];
$message = $_POST["message"];
$to = $_POST["user"];
$from = $user["id"];
$section = $_POST["section"];

$postId = addPost($header,$message,$to,$from,$section);
header("Location: /posts/post/$postId");
