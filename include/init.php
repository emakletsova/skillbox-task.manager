<?php
ini_set('session.gc_maxlifetime', 20 * 60);
session_set_cookie_params(20 * 60);
ini_set('session.cookie_lifetime', 20 * 60);
session_name("session_id");
session_start();