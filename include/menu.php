<?php
include($_SERVER['DOCUMENT_ROOT'] . "/include/main_menu.php");

function menuHeaderSorter($a, $b){
    return $a["sort"] <=> $b["sort"];
}

function menuFooterSorter($a, $b){
    return $b["sort"] <=> $a["sort"];
}

function getMenuItem($menu)
{
    foreach ($menu as $item)
        if (checkIsCurrentPath($item['path']))
            return $item;
}
function checkIsCurrentPath($path)
{
    return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) == $path;
}
//0-header; 1-footer
function printMenu($arrMenu, $sorter)
{
    usort($arrMenu, $sorter);
    include($_SERVER['DOCUMENT_ROOT'] . "/template/menu.php");
}

function cropText($text)
{
    return mb_strlen($text) > 15 
            ? mb_substr($text, 0, 12) . '...'
            : $text;
}
