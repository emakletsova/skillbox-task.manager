<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/include/db_pdo.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/include/db_user.php");

function getUserNewMessages($userId)
{
    $pdo = createPDO();
    $sql = "select * from messages 
            left join `users` on messages.`from` = users.id
            left join sections on sections.id=messages.section_id
            where messages.`to`='$userId' and messages.`read`=0";
    $messages  = $pdo->query($sql)->fetchAll();
    $pdo = null;
    return $messages ;
}
function getUserOldMessages($userId)
{
    $pdo = createPDO();
    $sql = "select * from messages 
            inner join `users` on messages.`to` = users.id
            inner join sections on sections.id=messages.section_id
            where messages.`to`='$userId'  and messages.`read`=1";
    $messages  = $pdo->query($sql)->fetchAll();
    $pdo = null;
    return $messages;
}

function getMessage($id)
{
    $pdo = createPDO();
    $sql = "select * from messages 
            inner join `users` ufrom on messages.`from` = ufrom.id
            where messages.id = $id";
    $messages  = $pdo->query($sql)->fetch();
    $pdo = null;
    return $messages ;
}
function getUserListToSend()
{
    $pdo = createPDO();
    $sql = "select * from users 
            inner join group_user gu on users.id = gu.user_id
    inner join `groups` on gu.group_id = `groups`.id
    where `groups`.readonly = 0";
    $users  = $pdo->query($sql)->fetchAll();
    $pdo = null;
    return $users ;
}
function getUserSections($userId)
{
    $pdo = createPDO();

    $sql = "SELECT s.name, s.parent_id, s.id, lc.color, lc.name color_name
            FROM sections AS s
            inner join colors lc on s.color_id = lc.id
            where s.created = '$userId'
            ORDER BY name
            ";
    $result  = $pdo->query($sql)->fetchAll();

    //https://stackoverflow.com/questions/4452472/category-hierarchy-php-mysql
    $refs = array();
    $list = array();

    foreach ($result as $row)
    {
        $ref = & $refs[$row['id']];

        $ref['id'] = $row['id'];
        $ref['parent_id'] = $row['parent_id'];
        $ref['name']      = $row['name'];
        $ref['color_name'] = $row['color_name'];
        $ref['color']     = $row['color'];

        if ($row['parent_id'] == 0)
        {
            $list[$row['id']] = & $ref;
        }
        else
        {
            $refs[$row['parent_id']]['children'][$row['id']] = & $ref;
        }
    }

    return $list;
}

function toUL(array $array)
{
    $html = '<ul>' . PHP_EOL;

    foreach ($array as $value)
    {
        $html .= '<li>' . $value['name'];
        if (!empty($value['children']))
        {
            $html .= toUL($value['children']);
        }
        $html .= '</li>' . PHP_EOL;
    }

    $html .= '</ul>' . PHP_EOL;

    return $html;
}
function toOptionList(array $array, int $depth = 0)
{
    $html = PHP_EOL;

    foreach ($array as $value)
    {
        $html .= '<option value="' . $value['id'] . '" style="color:'. $value["color"] .'">'
            . str_repeat("&nbsp;|&nbsp;", $depth) .
            $value["name"] . '</option>';
        if (!empty($value['children']))
        {

            $html .= toOptionList($value['children'], $depth+1);
        }
        $html .= PHP_EOL;
    }

    $html .= PHP_EOL;

    return $html;
}

function addPost($header, $message, $to, $from, $section)
{
    $pdo = createPDO();
    $sql = "insert into messages 
                    (id, section_id, body, created, `from`, `to`, `read`, header) 
            values (null, :section, :message, current_timestamp(), :from,  :to, 0, :header )";

    $req = $pdo->prepare($sql);
    $req->bindParam(":section", $section, PDO::PARAM_INT);
    $req->bindParam(":message", $message, PDO::PARAM_STR);
    $req->bindParam(":from", $from, PDO::PARAM_INT);
    $req->bindParam(":to", $to, PDO::PARAM_INT);
    $req->bindParam(":header", $header, PDO::PARAM_STR);
    $req->execute();
    $result = $pdo->lastInsertId();
    $pdo = null;
    return $result;
}
function setMessageRead($id)
{
    $pdo = createPDO();
    $sql = "update messages set `read`=1
            where messages.id = $id";
    $messages  = $pdo->prepare($sql)->execute();
    print_r($sql);
    $pdo = null;
}

