<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/include/db_pdo.php");

function checkUser($login, $pwd)
{
    $user = getUserByLogin($login);

    return $user !== false ? password_verify($pwd, $user["pwd_hash"]) : false;
}

function getUserByLogin($login)
{
    $pdo = createPDO();
    $login = $pdo->quote($login);
    $sql = "select * from users where email = $login";
    $user = $pdo->query($sql)->fetch();
    $pdo = null;
    return $user;
}
function getUserById($userId)
{
    $pdo = createPDO();
    $sql = "select * from users where id = '$userId'";
    $user = $pdo->query($sql)->fetch();
    $pdo = null;
    return $user;
}
function getUserGroups($userId)
{
    $pdo = createPDO();
    $sql = "select * from group_user 
            left join `groups` on group_user.group_id = `groups`.id
            where user_id='$userId' ";
    $groups = $pdo->query($sql)->fetchAll();
    $pdo = null;
    return $groups ;
}

function getValueOrDefault($user, $key, $default = "Не указано")
{
    return $user[$key] ?? $default;
}
