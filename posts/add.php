<?php
include($_SERVER['DOCUMENT_ROOT'] . "/include/init.php");

if (empty($_SESSION["auth"]))
    header("Location: /?login=yes");

ob_start();

include($_SERVER['DOCUMENT_ROOT'] . "/include/auth.php");

$title_text = "";
include($_SERVER['DOCUMENT_ROOT'] . "/include/menu.php");
$title = "Добавление сообщения";
include($_SERVER['DOCUMENT_ROOT'] . "/template/header.php");
include($_SERVER['DOCUMENT_ROOT'] . "/template/add.php");
include($_SERVER['DOCUMENT_ROOT'] . "/template/footer.php");

ob_end_flush();
