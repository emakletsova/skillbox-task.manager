<?php
$user = getUserById($_SESSION["userId"]);
?>
<div class="background-profile">
    <h3></h3>
    <table>
        <tr>
            <td>ФИО</td>
            <td><?= getValueOrDefault( $user, "fio") ?></td>
        </tr>
        <tr>
            <td>Почта</td>
            <td><?= getValueOrDefault( $user, "email") ?></td>
        </tr>
        <tr>
            <td>Телефон</td>
            <td><?= getValueOrDefault( $user, "phone") ?></td>
        </tr>
        <tr>
            <td>Согласие</td>
            <td><?= $user["allow_notify_email"] ? "Дано" : "Не дано" ?></td>
        </tr>
        <tr>
            <td>Группы</td>
            <td>
                <ul>
                    <?php
                    foreach (getUserGroups($user['id']) as $g)
                        echo "<li>" . $g['name'] . "</li>";
                    ?>
                </ul>
            </td>
        </tr>

    </table>

</div>