<?php

include($_SERVER['DOCUMENT_ROOT'] . "/include/db_posts.php");
$user = getUserById($_SESSION["userId"]);
?>
<div class="background-profile">
                    <?php
                    $show = false;
                    foreach (getUserGroups($user['id']) as $g) {
                        if (!$g['readonly']) {
                            $show = true;
                            break;
                        }
                    }
                        if (!$show) {
                            echo "<div> Вы сможете отправлять сообщения после прохождения модерации </div>";
                        }else {
                            ?>

        <h3>Непрочитанные сообщения</h3>
        <ul>
            <?php foreach (getUserNewMessages($user['id']) as $m){ ?>
                <li> <a href='/posts/post/<?= $m['id'] ?>'> <?= $m['header'] ?>  </a>  </li>
            <?php } ?>
        </ul>
        <h3>Прочитанные сообщения</h3>
        <ul>
            <?php foreach (getUserOldMessages($user['id']) as $m){ ?>
                <li> <a href='/posts/post/<?= $m['id'] ?>'> <?= $m['header'] ?>  </a>  </li>
            <?php } ?>
        </ul>
        <button onclick="location.href='/posts/add/';">Написать сообщение</button>
    <?php }

                    ?>
</div>