<?php
$user = getUserById($_SESSION["userId"]);
include($_SERVER['DOCUMENT_ROOT'] . "/include/db_posts.php");

$show = false;
foreach (getUserGroups($user['id']) as $g) {
    if (!$g['readonly']) {
        $show = true;
        break;
    }
}

if (!$show)
    header("Location: /posts");
?>
<div class="background-profile">
    <form method="POST" action="/include/post.add.action.php">
        <h3>Заголовок</h3>
        <input type="text" name="header" />
        <h3>Текст сообщения </h3>
        <input type="text" name="message" />
        <h3>Пользователь </h3>
        <select name="user">
            <?php foreach (getUserListToSend() as $u) { ?>
                <option value="<?= $u['id'] ?>"><?= $u['fio'] ?></option>
            <?php } ?>
        </select>
        <h3>Раздел сообщения </h3>
        <select name="section">
            <?= toOptionList(getUserSections($user["id"])) ?>
        </select>
        <br/>
        <br/>
        <input type="submit" value="Отправить" />
    </form>
</div>