<ul class="menu main-menu">
<?php
foreach ($arrMenu as $item){
?>
    <li>
        <a href="<?= $item['path'] ?>" class="<?= checkIsCurrentPath($item['path']) ? 'active' : '' ?>" >
            <?= cropText($item['title']) ?>
        </a>
    </li>
<?php } ?>
</ul>
